import sys
from PyQt5.QtWidgets import QApplication,QWidget,QVBoxLayout,QHBoxLayout,QPushButton,QLabel,QSpinBox,QTableWidget,QTableWidgetItem,QTabWidget
from PyQt5.QtGui import QPixmap
import numpy as np
import matplotlib.pyplot as plt
import networkx as nx 
from sokrmatr import sokrmatr 
 
class Example(QWidget):
    def __init__(self): 
        super(Example,self).__init__()
        self.initUI()
        global p
        global m
        global d
        global n
        p=np.zeros((3,3))
        m=np.zeros((3,3))
        d=np.zeros((3,3))
        n=0
        self.p1()
                 
    def initUI(self): #размеры окна
        self.left = 400
        self.top = 50
        self.width = 1300
        self.height = 1000
        self.setGeometry(self.left, self.top, self.width, self.height)
        
    def p1(self): #интерфейс    
        self.label = QLabel(self)
        self.label.setText("К-во узлов: ")
        self.spin = QSpinBox(self)
        self.spin.setMaximum(20)
        self.spin.setMinimum(1)
        self.spin.valueChanged.connect(self.set_table)
        self.tabs = QTabWidget(self)
        self.tabs.resize(600, 300)
        self.tabs1 = QWidget()
        self.tabs2 = QWidget()
        self.tabs3 = QWidget()
        self.tabs.addTab(self.tabs1,"Вероятность")
        self.tabs.addTab(self.tabs2,"Мат. ожидание")
        self.tabs.addTab(self.tabs3,"Дисперсия")
        self.Ptable = QTableWidget(self)
        self.Ptable.resize(500, 250)
        self.tabs1.layout = QVBoxLayout(self)
        self.tabs1.layout.addWidget(self.Ptable)
        self.tabs1.setLayout(self.tabs1.layout)
        self.Mtable = QTableWidget(self)
        self.Mtable.resize(500, 250)
        self.tabs2.layout = QVBoxLayout(self)
        self.tabs2.layout.addWidget(self.Mtable)
        self.tabs2.setLayout(self.tabs2.layout)
        self.Dtable = QTableWidget(self)
        self.Dtable.resize(500, 250)
        self.tabs3.layout = QVBoxLayout(self)
        self.tabs3.layout.addWidget(self.Dtable)
        self.tabs3.setLayout(self.tabs3.layout)
        self.btn_outfile = QPushButton("Вывод в файл", self)
        self.btn_outfile.clicked.connect(self.outfile)
        self.btn_outfile.setEnabled(False)
        self.btn_func = QPushButton("Вычислить", self)
        self.btn_func.clicked.connect(self.func)
        self.btn_func.setEnabled(False)
        self.btn_res = QPushButton("Результаты", self)
        self.btn_res.clicked.connect(self.result) 
        self.btn_res.setEnabled(False)
        self.btn_input = QPushButton("Ввод таблицы", self)
        self.btn_input.clicked.connect(self.input_table) 
        self.btn_graph = QPushButton("Граф", self)
        self.btn_graph.clicked.connect(self.draw_graph)
        self.btn_graph.setEnabled(False)
        self.layout1 = QHBoxLayout()
        self.layout1.addWidget(self.label)
        self.layout1.addWidget(self.spin)
        self.layout1.addStretch(1)
        self.layout1.addWidget(self.btn_input)
        self.layout1.addWidget(self.btn_func)
        self.layout1.addWidget(self.btn_graph)
        self.layout1.addWidget(self.btn_res)
        self.layout1.addWidget(self.btn_outfile)
        self.layout1.addStretch(1)
        self.layout2 = QHBoxLayout()
        self.layout2.addWidget(self.tabs) 
        self.layout3 = QVBoxLayout()
        self.layout3.addLayout(self.layout1)
        self.layout3.addLayout(self.layout2)
        self.setLayout(self.layout3)
    
    
    def outfile(self): #вывод матриц в файл
        np.savetxt('p.txt', p)
        np.savetxt('m.txt', m)
        np.savetxt('d.txt', d)
           
    def set_table(self): #таблица для ввода матриц 
        self.Ptable.setRowCount(int(self.spin.text()))
        self.Ptable.setColumnCount(int(self.spin.text()))
        self.Mtable.setRowCount(int(self.spin.text()))
        self.Mtable.setColumnCount(int(self.spin.text()))
        self.Dtable.setRowCount(int(self.spin.text()))
        self.Dtable.setColumnCount(int(self.spin.text()))
        for i in range(self.Ptable.rowCount()):
            for j in range(self.Ptable.rowCount()):
                self.Ptable.setItem(i,j,QTableWidgetItem(str(0)))
                self.Mtable.setItem(i,j,QTableWidgetItem(str(0)))
                self.Dtable.setItem(i,j,QTableWidgetItem(str(0)))
               
    def input_table(self): #ввод матриц
        global p
        global m
        global d
        global n
        n=self.Ptable.rowCount()
        p=np.zeros((n,n)) 
        m=np.zeros((n,n)) 
        d=np.zeros((n,n))
        for i in range(n):
            for j in range(n):       
                p[i][j]=float(self.Ptable.item(i, j).text())
                m[i][j]=float(self.Mtable.item(i, j).text())
                d[i][j]=float(self.Dtable.item(i, j).text())
        self.btn_graph.setEnabled(True)
        self.btn_func.setEnabled(True)
    
    def func(self): #вычисления
        global p
        global m
        global d
        global n 
        p,m,d=sokrmatr(n,p,m,d)
        self.btn_res.setEnabled(True)
        self.btn_outfile.setEnabled(True)
       
    def draw_graph(self): #визуализация графа
        global p
        global m
        global d
        global n
        G1 = nx.from_numpy_matrix(p,create_using=nx.DiGraph)
        pos1 = nx.spring_layout(G1)
        nx.draw_networkx(G1,pos1,node_color='red',node_size=1000,with_labels=True)
        nx.draw_networkx_edge_labels(G1, pos1)
        plt.savefig('graph')
        pixmap1 = QPixmap('graph.png')
        self.label3 = QLabel(self)
        self.label3.setPixmap(pixmap1)
        self.label3.resize(pixmap1.width(), pixmap1.height())
        self.layout3.addWidget(self.label3) 
        self.setLayout(self.layout3)
    
    def result(self):  #таблица результатов  
        self.label2 = QLabel(self)
        self.label2.setText("Результат: ")             
        self.tabr = QTabWidget(self)
        self.tabr.resize(400, 200)
        self.tabr1 = QWidget()
        self.tabr2 = QWidget()
        self.tabr3 = QWidget()
        self.tabr.addTab(self.tabr1,"Вероятность")
        self.tabr.addTab(self.tabr2,"Мат. ожидание")
        self.tabr.addTab(self.tabr3,"Дисперсия")
        self.Prtable = QTableWidget(self)
        self.Prtable.setColumnCount(3)
        self.Prtable.setRowCount(3)
        self.tabr1.layout = QVBoxLayout(self)
        self.tabr1.layout.addWidget(self.Prtable)
        self.tabr1.setLayout(self.tabr1.layout)
        self.Mrtable = QTableWidget(self)
        self.Mrtable.setColumnCount(3)
        self.Mrtable.setRowCount(3)
        self.tabr2.layout = QVBoxLayout(self)
        self.tabr2.layout.addWidget(self.Mrtable)
        self.tabr2.setLayout(self.tabr2.layout)
        self.Drtable = QTableWidget(self)
        self.Drtable.setColumnCount(3)
        self.Drtable.setRowCount(3)
        self.tabr3.layout = QVBoxLayout(self)
        self.tabr3.layout.addWidget(self.Drtable)
        self.tabr3.setLayout(self.tabr3.layout)
        for i in range(3):
            for j in range(3):
                self.Prtable.setItem(i,j,QTableWidgetItem(str(p[i][j])))
                self.Mrtable.setItem(i,j,QTableWidgetItem(str(m[i][j])))
                self.Drtable.setItem(i,j,QTableWidgetItem(str(d[i][j]))) 
        self.layout1.addWidget(self.label2)
        self.layout2.addWidget(self.tabr)
        self.layout3.addLayout(self.layout1)
        self.layout3.addLayout(self.layout2)            
        self.btn_res.setEnabled(False)
        self.btn_outfile.setEnabled(True)
        self.btn_graph.setEnabled(False)
        self.setLayout(self.layout3)  
         
if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Example()
    ex.show()
    sys.exit(app.exec_())
