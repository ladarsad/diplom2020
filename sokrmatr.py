def sokrmatr(n,p1,m1,d1):
    import numpy as np
    from math import sqrt,ceil
    a=n*n
    while(a>9):                 
        k=1
        if p1[k][k]!=0:
                for j in range(n):
                    p1[k][j]=p1[k][j]/(1-p1[k][k])
                    m1[k][j]=m1[k][j]+m1[k][k]*p1[k][k]/(1-p1[k][k])
                    d1[k][j]=d1[k][j]+(d1[k][k]*p1[k][k]+m1[k][k]*m1[k][k]*p1[k][k])/(1-p1[k][k])
                p1[k][k]=0 
                m1[k][k]=0
                d1[k][k]=0
        p2=np.copy(p1)
        m2=np.copy(m1)
        d2=np.copy(d1)
        for l in range(n):
            if p1[l][k]!=0:
                for i in range(n):
                    if p1[k][i]!=0 and i!=k:
                        if p1[l][i]==0:
                            p2[l][i]=p1[l][k]*p1[k][i]
                            m2[l][i]=m1[l][k]+m1[k][i]
                            d2[l][i]=d1[l][k]+d1[k][i]
                        else:
                            p2[l][i]=p1[l][i]+p1[l][k]*p1[k][i]
                            m2[l][i]=(m1[l][i]*p1[l][i]+p1[l][k]*p1[k][i]*(m1[l][k]+m1[k][i]))/p2[l][i]
                            d2[l][i]=(d1[l][i]*p1[l][i]+p1[l][k]*p1[k][i]*(d1[l][k]+d1[k][i]) + m1[l][i]*m1[l][i]*p1[l][i]+p1[l][k]*p1[k][i]*(m1[l][k]+m1[k][i])**2) /p2[l][i] - m2[l][i]**2
        p1=np.delete(p1,(1),axis=0)
        p1=np.delete(p1,(1),axis=1)
        p2=np.delete(p2,(1),axis=0)
        p2=np.delete(p2,(1),axis=1)
        p1=np.copy(p2)    
        m1=np.delete(m1,(1),axis=0)
        m1=np.delete(m1,(1),axis=1)
        m2=np.delete(m2,(1),axis=0)
        m2=np.delete(m2,(1),axis=1)
        m1=np.copy(m2)    
        d1=np.delete(d1,(1),axis=0)
        d1=np.delete(d1,(1),axis=1)
        d2=np.delete(d2,(1),axis=0)
        d2=np.delete(d2,(1),axis=1)
        d1=np.copy(d2)        
        n-=1
        a=np.size(p1)
    k=0
    if p1[k][k]!=0:
        for j in range(1,n):
            p1[k][j]=p1[k][j]/(1-p1[k][k])
            m1[k][j]=m1[k][j]+m1[k][k]*p1[k][k]/(1-p1[k][k])
            d1[k][j]=d1[k][j]+(d1[k][k]*p1[k][k]+m1[k][k]*m1[k][k]*p1[k][k])/(1-p1[k][k])
        p1[k][k]=0  
        m1[k][k]=0
        d1[k][k]=0   
    return p1,m1,d1
   
    
    
    
